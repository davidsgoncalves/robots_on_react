// src/routes.js
import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider, connect } from 'react-redux';
import store from '../storesConfig';

import { aboutShape, apiStatusShape } from '../patterns/shapes';
import { aboutDefaultProps, apiStatusDefaultProps } from '../patterns/defaultProps';

import App from './App/App';
import About from './About/index';
import NotFound from './NotFound/index';
import AppNavbar from './App/AppNavbar';

const Routes = (props) => {
  const { about, apiStatus, dispatch } = props;

  return (
    <Router {...props}>
      <div>
        <AppNavbar apiStatus={apiStatus} dispatch={dispatch} />
        <Switch>
          <Route path="/" exact>
            <App />
          </Route>
          <Route path="/about">
            <About {...about} dispatch={dispatch} />
          </Route>
          <Route path="*" component={NotFound} />
        </Switch>
      </div>
    </Router>
  );
};

Routes.propTypes = {
  about: PropTypes.shape(aboutShape),
  apiStatus: PropTypes.shape(apiStatusShape),
  dispatch: PropTypes.func.isRequired,
};

Routes.defaultProps = {
  about: aboutDefaultProps,
  apiStatus: apiStatusDefaultProps,
};

function mapStateToProps(state) {
  return {
    about: state.AboutReducer.toJS(),
    apiStatus: state.ApiStatusPanel.toJS(),
  };
}

const ConnectedComponent = connect(mapStateToProps)(Routes);

export default function Root(props) {
  return <Provider store={store}><ConnectedComponent {...props} /></Provider>;
}
