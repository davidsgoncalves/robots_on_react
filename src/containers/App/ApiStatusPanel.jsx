import React from 'react';
import PropTypes from 'prop-types';
import { Chip } from 'material-ui';

import { checkStatusReq } from './ApiStatusPanelReducer';
import statusIcon from './statusIcon';
import StatusAvatar from '../../components/StatusAvatar/StatusAvatar';

export default function ApiStatusPanel({ apiStatus, dispatch }) {
  setInterval(() => dispatch(checkStatusReq()), 3000);

  const { status } = apiStatus;

  return (
    <Chip avatar={<StatusAvatar status={statusIcon(status)} />} label="Status da API" />
  );
}

ApiStatusPanel.propTypes = {
  apiStatus: PropTypes.shape({
    status: PropTypes.number,
  }),
  dispatch: PropTypes.func.isRequired,
};

ApiStatusPanel.defaultProps = {
  apiStatus: { status: null },
};

