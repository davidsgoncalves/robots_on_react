import Immutable from 'immutable';
import $ from 'jquery';

const CHECK_STATUS = 'api_status/CHECK_STATUS';

const DEFAULT_STATE = Immutable.fromJS({
  status: null,
});

export default function reducer(state = DEFAULT_STATE, action = {}) {
  switch (action.type) {
    case CHECK_STATUS:
      return state.set('status', action.data);
    default:
      return state;
  }
}

function checkStatus(data) {
  return { type: CHECK_STATUS, data };
}

export function checkStatusReq() {
  return (dispatch) => {
    $.ajax({
      url: 'https://robots-on-rails-api.herokuapp.com/status',
      method: 'get',
    })
      .done((data, textStatus, xhr) => dispatch(checkStatus(xhr.status)))
      .fail((data, textStatus, xhr) => dispatch(checkStatus(xhr.status)));
  };
}
