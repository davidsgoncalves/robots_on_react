export default function statusIcon(status) {
  let statusIconString;

  if (status === null) {
    statusIconString = 'Waiting';
  } else if (status === 204) {
    statusIconString = 'Success';
  } else {
    statusIconString = 'Error';
  }

  return statusIconString;
}
