import React from 'react';
import PropTypes from 'prop-types';

import { AppBar, Toolbar, Typography, Button } from 'material-ui';
import { LinkContainer } from 'react-router-bootstrap';
import { withStyles } from 'material-ui/styles';
import { teal } from 'material-ui/colors';

import ApiStatusPanel from './ApiStatusPanel';
import { apiStatusDefaultProps } from '../../patterns/defaultProps';

const styles = () => ({
  wrapper: {
    width: '100%',
  },
  navbar: {
    backgroundColor: teal[500],
  },
  title: {
    margin: '0 10px',
  },
});

function AppNavbar(props) {
  const { classes, dispatch, apiStatus } = props;
  return (
    <div className={classes.wrapper}>
      <AppBar position="static" className={classes.navbar}>
        <Toolbar>
          <Typography type="title" color="inherit" className={classes.title}>
            Robots on React
          </Typography>
          <LinkContainer exact to="/">
            <Button color="contrast">Inicio</Button>
          </LinkContainer>
          <LinkContainer to="/about">
            <Button color="contrast">Sobre</Button>
          </LinkContainer>
          <ApiStatusPanel apiStatus={apiStatus} dispatch={dispatch} />
        </Toolbar>
      </AppBar>
    </div>
  );
}

AppNavbar.propTypes = {
  classes: PropTypes.object.isRequired,
  apiStatus: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
};

AppNavbar.defaultProps = {
  apiStatus: apiStatusDefaultProps,
};

export default withStyles(styles)(AppNavbar);
