// src/components/NotFound/index.js
import React from 'react';

import './style.css';

export default function NotFound() {
  return (
    <div className="NotFound">
      <h1>
        404 <small>Not Found :(</small>
      </h1>
    </div>
  );
}
