

/* eslint-env jasmine */

import React from 'react';
import { render } from 'enzyme';
import NotFound from '../index';

describe('First Test', () => {
  it('Check text', () => {
    expect(render(<NotFound />).text()).toEqual('404 Not Found :(');
  });
});
