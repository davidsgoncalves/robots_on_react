// src/components/About/index.js
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { addNumber } from './aboutReducer';
import './style.css';

export default class About extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.props.dispatch(addNumber());
  }

  render() {
    const { number } = this.props;
    return (
      <div className="About">
        <div>
          About
          <p>{number} </p>
          <button color="danger" onClick={this.onClick}>Button</button>
        </div>
      </div>
    );
  }
}

About.propTypes = {
  number: PropTypes.number.isRequired,
  dispatch: PropTypes.func.isRequired,
};
