// src/index.js
import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';

import Routes from './containers/';

ReactDOM.render(
  React.createElement(Routes, {}, null),
  document.getElementById('root'),
);
