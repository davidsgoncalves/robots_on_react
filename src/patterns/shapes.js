import PropTypes from 'prop-types';

export const aboutShape = {
  number: PropTypes.number,
};

export const apiStatusShape = {
  status: PropTypes.number,
};
