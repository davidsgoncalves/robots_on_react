import React from 'react';
import PropTypes from 'prop-types';

import { Avatar } from 'material-ui';
import { withStyles } from 'material-ui/styles';
import { green, red, grey } from 'material-ui/colors';
import { Done, Clear, MoreHoriz } from 'material-ui-icons';

const styles = () => ({
  successAvatar: {
    color: '#fff',
    backgroundColor: green[500],
  },
  errorAvatar: {
    color: '#fff',
    backgroundColor: red[500],
  },
  waitingAvatar: {
    color: '#fff',
    backgroundColor: grey[500],
  },
});

function StatusAvatar({ status, classes }) {
  if (status === 'Success') {
    return (<Avatar className={classes.successAvatar}><Done /></Avatar>);
  } else if (status === 'Error') {
    return (<Avatar className={classes.errorAvatar}><Clear /></Avatar>);
  } else if (status === 'Waiting') {
    return (<Avatar className={classes.waitingAvatar}><MoreHoriz /></Avatar>);
  }
}

StatusAvatar.propTypes = {
  status: PropTypes.oneOf([
    'Success', 'Error', 'Waiting',
  ]).isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(StatusAvatar);
