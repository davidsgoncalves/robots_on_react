import { combineReducers } from 'redux';

import AboutReducer from './containers/About/aboutReducer';
import ApiStatusPanel from './containers/App/ApiStatusPanelReducer';

const rootReducer = combineReducers({
  AboutReducer,
  ApiStatusPanel,
});

export default rootReducer;
